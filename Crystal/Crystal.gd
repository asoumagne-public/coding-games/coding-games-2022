extends RigidBody2D

export var life = 100
export var strength = 5

signal life_changed(life)

var isHurting = false
var deathScene = preload("res://Death/Death.tscn")

func hurt(amount):
	$HurtSound.play()
	if !isHurting:
		isHurting = true
		life -= (amount / strength)
		emit_signal("life_changed", life)
		$AnimationPlayer.play("Hurt")
		
		if life <= 0:
			destroy()

func destroy():
	$".".visible = false
	var death = deathScene.instance()
	death.global_position = global_position
	death.loot = false
	get_tree().get_root().add_child(death)
	queue_free()
	Signals.emit_signal("crystal_destroyed")

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "Hurt":
		$AnimationPlayer.play("Idle")
		isHurting = false
