extends Node2D

var wave = 0
var capacity = 5
var isRunning = false
var capacity_multiplier = 1.75

var hudScene = preload("res://GUI/HUD/HUD.tscn")
var gameOverScene = preload("res://GUI/GameOver/GameOver.tscn")
var menuScene = preload("res://GUI/Menu/Menu.tscn")

var gameOver
var mob_left

signal wave_changed(wave)
signal wave_started
signal mob_quantity(quantity)

func _ready():
	var __: int
	__ = Signals.connect("mob_killed", self, "_mob_killed")
	__ = Signals.connect("crystal_destroyed", self, "game_over")
	__ = Signals.connect("player_killed", self, "game_over")
	__ = Signals.connect("game_restart", self, "game_restart")
	__ = Signals.connect("main_theme_state_changed", self, "main_theme_state_changed")
	
	new_wave()

func _process(_delta):
	_pause()
	
	if Input.is_action_pressed("ready") && !isRunning:
		emit_signal("mob_quantity", capacity)
		emit_signal("wave_started")
		isRunning = true
		$Portals.spawn(capacity, wave)
	
func game_over():
	get_tree().call_group("mobs", "queue_free")
	get_tree().call_group("all_xp", "queue_free")
	get_tree().call_group("turrets", "queue_free")
	
	if $Player:
		$Player.queue_free()
	if $Crystal:
		$Crystal.queue_free()
	$Portals.queue_free()
	$HUD.queue_free()
	
	gameOver = gameOverScene.instance()
	gameOver.last_wave = wave
	get_tree().get_root().add_child(gameOver)
	
func _on_Portals_wave_ended():
	new_wave()

func new_wave():
	if wave > 0: 
		capacity = floor(capacity * capacity_multiplier) 
	
	wave += 1
	mob_left = capacity
	isRunning = false
	$Portals/SpawnTimer.wait_time -= 0.02
	emit_signal("wave_changed", wave)
	emit_signal("mob_quantity", capacity)
	
func game_restart():
	return get_tree().reload_current_scene()
	
func _on_Menu_restart():
	return get_tree().reload_current_scene()

func _mob_killed():
	mob_left -= 1
	emit_signal("mob_quantity", mob_left)

func _pause():
	if Input.is_action_just_pressed("pause"):
		get_tree().paused = true
		
		var menu = menuScene.instance()
		menu.connect("restart", self, "_on_Menu_restart")
		add_child(menu)

func main_theme_state_changed(state) -> void:
	match state:
		States.Sound.MUTE:
			$AudioStreamPlayer2D.stop()
		States.Sound.LOW:
			if !$AudioStreamPlayer2D.playing:
				$AudioStreamPlayer2D.play()
			$AudioStreamPlayer2D.volume_db = -25
		States.Sound.HIGH:
			if !$AudioStreamPlayer2D.playing:
				$AudioStreamPlayer2D.play()
			$AudioStreamPlayer2D.volume_db = 0
