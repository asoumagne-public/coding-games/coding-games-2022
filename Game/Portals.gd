extends Node2D

# Quantity of ennemy to spawn
export var spawn_capacity = 1
# quantity already spawned
export var spawn_quantity = 0

signal wave_ended

var portals;
var spawn_ended = false
var current_wave;

func _ready():
	randomize()
	portals = [$PortalLeft, $PortalRight]
	
func _process(_delta):
	if spawn_ended && get_tree().get_nodes_in_group("mobs").size() == 0:
		spawn_ended = false
		emit_signal("wave_ended")

func spawn(quantity, wave):
	current_wave = wave
	spawn_ended = false
	spawn_capacity = quantity
	spawn_quantity = 0
	$SpawnTimer.start()
	$SpawnSound.play()
	$WaveSound.play()
	
func _on_SpawnTimer_timeout():
	portals[randi() % 2].spawn(current_wave);
	spawn_quantity+=1;
	
	if spawn_capacity <= spawn_quantity:
		$SpawnTimer.stop()
		spawn_ended = true
