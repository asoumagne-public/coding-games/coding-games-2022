class_name Player
extends KinematicBody2D

var velocity = Vector2.ZERO
var gravity = 10
var life = 100
var xp = 0
var level = 1
var total_turret_cost = 0

export var xp_by_level = 100
export var max_turret_cost = 5

onready var animations = $AnimationPlayer
onready var states = $FSM
onready var pivot = $Pivot

func _ready() -> void:
	states.init(self)
	emit_signal("life_changed", life)

func _unhandled_input(event: InputEvent) -> void:
	states.input(event)

func _physics_process(delta: float) -> void:
	states.physics_process(delta)

func _process(delta: float) -> void:
	total_turret_cost = 0
	for turret in get_tree().get_nodes_in_group("turrets"):
		total_turret_cost += turret.cost

	emit_signal("turret_available", int(max_turret_cost - total_turret_cost) / 5)
	
	states.process(delta)

signal life_changed(life)
signal xp_changed(xp, max_xp)
signal level_changed(level)
signal turret_available(count)

var orbScene = preload("res://Player/Orb/Orb.tscn")
var turretScene = preload("res://Turret/Turret.tscn")
var deathScene = preload("res://Death/Death.tscn")

func create_turret():
		var turret = turretScene.instance();
		
		var collider = $Pivot/ActionPlace.get_collider()
		if collider and collider.is_in_group('turrets'):
			collider.kill()
			return

		if turret.cost + total_turret_cost <= max_turret_cost:
			turret.global_position = $Pivot/TurretSpawn.global_position
			turret.power = 1
#			turret.strength = effective_strength
			turret.direction = pivot.scale.x
			get_tree().get_root().add_child(turret)


func create_orb():
	var orb = orbScene.instance()
	var flip = pivot.scale.x > 0
	orb.global_position = global_position
	orb.direction = flip
	orb.attack = 200
	orb.power = 1
	orb.position.y -= 8
	if flip: 
		orb.position.x -= 20 
		orb.direction = -1
	else: 
		orb.position.x += 20 
		orb.direction = 1
	get_tree().get_root().add_child(orb)

func hurt(amount):
	life -= amount

	if life <= 0:
		kill()
	else:
		$AnimationPlayer.play("Hurt")
		$Sounds/Hurt.play()
	
	emit_signal("life_changed", life)

func kill():
	$".".visible = false
	var death = deathScene.instance()
	death.global_position = global_position
	death.loot = false
	get_tree().get_root().add_child(death)

	yield(get_tree().create_timer(0.8), "timeout")
	Signals.emit_signal("player_killed")
	queue_free()

func collect(amount):
	xp += amount

	var max_xp = xp_by_level * level;
	if xp >= max_xp:
		xp = xp - max_xp
		level += 1
		$Sounds/LevelUp.play()
		emit_signal("level_changed", level)

	emit_signal("xp_changed", xp, max_xp)
