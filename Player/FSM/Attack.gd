extends PlayerState

export (NodePath) var jump_node
export (NodePath) var idle_node

onready var jump_state: PlayerState = get_node(jump_node)
onready var idle_state: PlayerState = get_node(idle_node)
onready var timer = $Timer

func exit() -> void:
	timer.stop()

func process(_delta: float) -> BaseState:
	if Input.is_action_pressed("move_left"):
		player.pivot.scale.x = 1
	elif Input.is_action_pressed("move_right"):
		player.pivot.scale.x = -1
	
	if !player.is_on_floor():
		player.velocity.y += player.gravity
		player.velocity = player.move_and_slide(player.velocity, Vector2.UP)
	
	if Input.is_action_just_pressed("jump") and player.is_on_floor():
		return jump_state
	elif Input.is_action_pressed("attack"):
		if timer.is_stopped():
			player.animations.seek(0)
			player.animations.play("Attack")
			timer.start()
	elif player.is_on_floor():
			return idle_state
	
	return null

func _on_Timer_timeout():
	player.create_orb()
