extends PlayerState

export (NodePath) var idle_node
export (NodePath) var walk_node
export (NodePath) var jump_node

onready var idle_state: PlayerState = get_node(idle_node)
onready var walk_state: PlayerState = get_node(walk_node)
onready var jump_state: PlayerState = get_node(jump_node)

onready var timer = $Timer

func enter() -> void:
	.enter()
	timer.start()

func exit() -> void:
	timer.stop()

func process(_delta: float) -> BaseState:
	if Input.is_action_pressed("move_left") \
	or Input.is_action_pressed("move_right"):
		timer.stop()
		return walk_state
		
	if Input.is_action_pressed("jump"):
		timer.stop()
		return jump_state
	
	return null

func _on_Timer_timeout():
	player.create_turret()
	
func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == animation_name:
		player.states.change_state(idle_state)
