class_name MoveState
extends PlayerState

export (float) var move_speed = 200.0
export (NodePath) var jump_node
export (NodePath) var fall_node
export (NodePath) var idle_node
export (NodePath) var walk_node
export (NodePath) var run_node
export (NodePath) var attack_node

onready var jump_state: PlayerState = get_node(jump_node)
onready var fall_state: PlayerState = get_node(fall_node)
onready var idle_state: PlayerState = get_node(idle_node)
onready var walk_state: PlayerState = get_node(walk_node)
onready var run_state: PlayerState = get_node(run_node)
onready var attack_state: PlayerState = get_node(attack_node)

func input(_event: InputEvent) -> BaseState:
	if Input.is_action_just_pressed("jump"):
		return jump_state
		
	if Input.is_action_just_pressed("attack"):
		return attack_state

	return null

func process(_delta: float) -> BaseState:
	if !player.is_on_floor():
		return fall_state

	var move = get_movement_input()
	if move < 0:
		player.pivot.scale.x = 1
		player.velocity.x -= move_speed
	elif move > 0:
		player.pivot.scale.x = -1
	
	player.velocity.y = player.gravity
	player.velocity.x = move * move_speed
	player.velocity = player.move_and_slide(player.velocity, Vector2.UP, true)
	
	if move == 0:
		return idle_state
		
	return null

func get_movement_input() -> int:
	if Input.is_action_pressed("move_left"):
		return -1
	elif Input.is_action_pressed("move_right"):
		return 1
	
	return 0
