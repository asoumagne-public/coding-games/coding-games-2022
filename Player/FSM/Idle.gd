extends PlayerState

export (NodePath) var jump_node
export (NodePath) var fall_node
export (NodePath) var walk_node
export (NodePath) var run_node
export (NodePath) var attack_node
export (NodePath) var turret_node

onready var jump_state: PlayerState = get_node(jump_node)
onready var fall_state: PlayerState = get_node(fall_node)
onready var walk_state: PlayerState = get_node(walk_node)
onready var run_state: PlayerState = get_node(run_node)
onready var attack_state: PlayerState = get_node(attack_node)
onready var turret_state: PlayerState = get_node(turret_node)

func enter() -> void:
	.enter()
	player.velocity.x = 0

func input(_event: InputEvent) -> BaseState:
	if Input.is_action_just_pressed("jump"):
		return jump_state
		
	if Input.is_action_just_pressed("turret"):
		return turret_state
	
	return null

func process(_delta: float) -> BaseState:
	player.velocity.y += player.gravity
	player.velocity = player.move_and_slide(player.velocity, Vector2.UP, true)
	
	if Input.is_action_pressed("move_left") or Input.is_action_pressed("move_right"):
		if Input.is_action_pressed("run"):
			return run_state
		return walk_state
	if Input.is_action_pressed("attack"):
		return attack_state

	if !player.is_on_floor():
		return fall_state
	return null
