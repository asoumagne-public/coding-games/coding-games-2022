class_name PlayerState
extends BaseState

export (String) var animation_name

var player: Player

func enter() -> void:
	player.animations.play(animation_name)
