extends PlayerState

export (float) var move_speed = 60.0
export (NodePath) var run_node
export (NodePath) var walk_node
export (NodePath) var idle_node

onready var run_state: PlayerState = get_node(run_node)
onready var walk_state: PlayerState = get_node(walk_node)
onready var idle_state: PlayerState = get_node(idle_node)

func process(_delta: float) -> BaseState:
	var move = 0
	if Input.is_action_pressed("move_left"):
		move = -1
		player.pivot.scale.x = 1
	elif Input.is_action_pressed("move_right"):
		move = 1
		player.pivot.scale.x = -1
	
	player.velocity.x = move * move_speed
	player.velocity.y += player.gravity
	player.velocity = player.move_and_slide(player.velocity, Vector2.UP, true)

	if player.is_on_floor():
		if move != 0:
			if Input.is_action_pressed("run"):
				return run_state
			return walk_state
		else:
			return idle_state
	return null
