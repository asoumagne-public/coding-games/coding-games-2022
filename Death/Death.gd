extends Node2D

export var loot = true

var xpScene = preload("res://XP/XP.tscn")

func _on_AnimationPlayer_animation_finished(_anim_name):
	queue_free()

func _on_AnimationPlayer_animation_started(_anim_name):
	if loot:
		var xp = xpScene.instance()
		xp.global_position = global_position
		get_tree().get_root().call_deferred("add_child", xp)
