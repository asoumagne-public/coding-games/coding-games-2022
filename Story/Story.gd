extends CanvasLayer

func _ready():
	$AnimationPlayer.play("Step1")
	$Step1/Step1Timer.start()

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "Step1_end":
		$AnimationPlayer.play("Step2")
		$Step2/Step2Timer.start()
	if anim_name == "Step2_end":
		$AnimationPlayer.play("Step3")
		$Step3/Step3Timer.start()
	if anim_name == "Step3_end":
		$AnimationPlayer.play("Step4")
		$Step4/Step4Timer.start()
	if anim_name == "Step4_end":
		return get_tree().change_scene("res://Game/Game.tscn")

func _on_Step1Timer_timeout():
	$AnimationPlayer.play("Step1_end")

func _on_Step2Timer_timeout():
	$AnimationPlayer.play("Step2_end")
	
func _on_Step3Timer_timeout():
	$AnimationPlayer.play("Step3_end")

func _on_Step4Timer_timeout():
	$AnimationPlayer.play("Step4_end")

func _on_Button_pressed():
	return get_tree().change_scene("res://Game/Game.tscn")
