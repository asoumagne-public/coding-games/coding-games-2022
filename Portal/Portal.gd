extends StaticBody2D

export var direction = 1

var mobScene = preload("res://Mob/Mob.tscn")

func spawn(level):
	var mob = mobScene.instance()
	mob.global_position = global_position
	mob.direction = direction
	mob.level = level

	if direction > 0:
		mob.position.x += 30
	elif direction < 0: 
		$Sprite.flip_h = true
		mob.position.x -= 30

	get_tree().get_root().add_child(mob)
