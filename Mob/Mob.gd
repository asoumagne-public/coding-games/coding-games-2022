extends KinematicBody2D

export var speed = 75
export var direction = 1
export var gravity = 10
export var life = 30
export var strength = 1
export var attack = 30
export var power = 1
export var level = 1
export var bonus_strength_by_level = 10
export var bonus_power_by_level = 5
export var bonus_speed_by_level = 10

var floor_normal = Vector2.UP
var movement = Vector2.ZERO
var targets
var effective_strength
var effective_power
var effective_speed

var deathScene = preload("res://Death/Death.tscn")

func _process(_delta):
	effective_strength = strength + (level * bonus_strength_by_level / 10)
	effective_power = power + (level * bonus_power_by_level / 10)
	effective_speed = speed + (level * bonus_speed_by_level)
	targets = $MobWeapon.get_overlapping_bodies()
	movement.x = 0
	_right()
	_left()
	_gravity()
	_attack()
	_update()

func _left():
	if !targets && direction < 0:
		$AnimationPlayer.play("Walk")
		movement.x -= effective_speed

func _right():
	if !targets && direction > 0:
		$AnimationPlayer.play("Walk")
		$Sprite.flip_h = true
		movement.x += effective_speed

func _attack():
	if $AttackDelayTimer.time_left == 0 && targets:
		if direction > 0:
			$Sprite.flip_h = true
		$AttackDelayTimer.start()
		$AttackTimer.start()
		$AnimationPlayer.play("Attack")

func _gravity():
	if is_on_floor():
		movement.y = gravity
	else:
		movement.y += gravity

func _update():
	movement = move_and_slide(movement, floor_normal)

func hurt(amount):
	life -= (amount / effective_strength)
	$EntityLife.updateLife(life)
	
	if life <= 0:
		kill()
		
func kill():
	var death = deathScene.instance()
	death.global_position = global_position
	get_tree().get_root().add_child(death)
	Signals.emit_signal("mob_killed")
	queue_free()

func _on_AttackTimer_timeout():
	for body in targets:
		if body.has_method("hurt"):
			body.hurt(attack * effective_power)
