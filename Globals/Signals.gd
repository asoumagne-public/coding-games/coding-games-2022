extends Node

signal mob_killed()
signal crystal_destroyed()
signal player_killed()

signal game_restart()
signal main_theme_state_changed(state)
