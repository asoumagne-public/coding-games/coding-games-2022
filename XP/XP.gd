extends RigidBody2D

export var value = 10

func _on_Area2D_body_entered(body):
	if body.has_method("collect"):
		body.collect(value)
		$AudioStreamPlayer2D.play()
		$".".visible = false
		
func _on_AudioStreamPlayer2D_finished():
	queue_free()
