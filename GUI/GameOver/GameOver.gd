extends CanvasLayer

var last_wave: int

func _ready():
	$Score.text += ' ' + String(last_wave)
	pass

func _on_Restart_pressed():
	Signals.emit_signal("game_restart")
	queue_free()
