extends CanvasLayer

signal restart

func _ready():
	_update_audio_icon(States.main_theme)

func _process(_delta):
	_resume()

func _on_Continue_pressed():
	get_tree().paused = false
	queue_free()

func _on_Quit_pressed():
	get_tree().quit()

func _on_Restart_pressed():
	get_tree().paused = false
	Signals.emit_signal("game_restart")
	
func _resume():
	if Input.is_action_just_pressed("pause"):
		get_tree().paused = false
		queue_free()

func _on_Audio_pressed():
	States.main_theme -= 1
	if States.main_theme < States.Sound.MUTE:
		States.main_theme = States.Sound.HIGH
		
	_update_audio_icon(States.main_theme)
	Signals.emit_signal("main_theme_state_changed", States.main_theme)
	
func _update_audio_icon(state):
	match state:
		States.Sound.MUTE:
			$Audio.icon = load("res://GUI/Menu/volume-mute.png")
		States.Sound.LOW:
			$Audio.icon = load("res://GUI/Menu/volume-low.png")
		States.Sound.HIGH:
			$Audio.icon = load("res://GUI/Menu/volume-high.png")
