extends Node

func _on_Crystal_life_changed(life):
	$GameInfo/Crystal/Life.value = life

func _on_Player_life_changed(life):
	$PlayerInfo/PlayerLife.value = life

func _on_Player_xp_changed(xp, max_xp):
	$PlayerInfo/XP.value = xp
	$PlayerInfo/XP.max_value = max_xp

func _on_Player_level_changed(level):
	$PlayerInfo/Level.text = String(level)
	
func _on_Player_turret_available(count):
	$PlayerInfo/TurretAvailable/Label.text = String(count)

func _on_Game_wave_changed(wave):
	$GameInfo/Wave.text = String(wave)
	$GameInfo/NextWaveMessage.visible = true

func _on_Game_wave_started():
	$GameInfo/NextWaveMessage.visible = false

func _on_Game_mob_quantity(quantity):
	$GameInfo/MobCount/Label.text = String(quantity)
