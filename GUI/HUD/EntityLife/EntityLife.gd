extends TextureProgress


onready var entity = get_parent()

func _ready():
	max_value = entity.life
	value = entity.life

func updateLife(life):
	value = life
	visible = true
