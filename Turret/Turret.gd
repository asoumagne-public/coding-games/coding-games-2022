extends RigidBody2D

export var cost = 5
export var life = 100
export var attack = 20
export var strength = 1
export var power = 1
export var direction = 1

var arrowScene = preload("res://Turret/Arrow/Arrow.tscn")
var deathScene = preload("res://Death/Death.tscn")
var isAttacking = false

func _process(_delta):
	if direction == -1:
		$Sprite.flip_h = true
		$Range/CollisionShape2D.position.x = 215
	_attack()
	
func _attack():
	if !isAttacking && $Range.get_overlapping_bodies().size() > 0:
		isAttacking = true
		$AttackTimer.start()
		$AnimationPlayer.play("Attack")

func createArrow():
	var arrow = arrowScene.instance()
	var flip = $Sprite.flip_h
	arrow.global_position = global_position
	arrow.direction = flip
	arrow.attack = attack
	arrow.power = power
	if !flip: 
		arrow.position.x -= 40 
		arrow.direction = -1
	else: 
		arrow.position.x += 40 
		arrow.direction = 1
	get_tree().get_root().add_child(arrow)

func _on_AttackTimer_timeout():
	createArrow()

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "Attack":
		isAttacking = false
	elif anim_name == "Hurt":
		$AnimationPlayer.stop()
	
func hurt(amount):
	life -= amount
	$HurtSound.play()
	$AnimationPlayer.play("Hurt")
	
	if $EntityLife:
		$EntityLife.updateLife(life)
	
	if life <= 0:
		kill()
		
func kill():
	visible = false
	$CollisionShape2D.disabled = true
	var death = deathScene.instance()
	death.global_position = global_position
	death.loot = false
	get_tree().get_root().add_child(death)
	
	yield(get_tree().create_timer(0.8), "timeout")
	queue_free()
