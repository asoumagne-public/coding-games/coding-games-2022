extends KinematicBody2D

export var move_speed = 80
export var movement = Vector2.ZERO
export var attack = 10
export var power = 1
export var direction = 1 # 1: Right, -1: Left
var floor_normal = Vector2.UP

func _ready():
	$LifeTimer.start()

func _process(_delta):
	var flip = direction == -1;
	$Sprite.flip_h = flip
	if flip: $Area2D.position.x = -25 
	else: $Area2D.position.x = 13
	movement.x += (move_speed * direction)
	return move_and_slide(movement, floor_normal)

func _on_Area2D_body_entered(body):
	if body.has_method("hurt") && body.name != "Player" && body.name != "Arrow":
		body.hurt(attack * power)
	queue_free()

func _on_LifeTimer_timeout():
	queue_free()
